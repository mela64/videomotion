python vprocessor.py --run ../data/skater.avi --out exp/skater1 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater2 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater3 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater4 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater5 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater6 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater7 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater8 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater9 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater10 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater11 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater12 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater13 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater14 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater15 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater16 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- 5 x 5 ------

python vprocessor.py --run ../data/skater.avi --out exp/skater17 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater18 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater19 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater20 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater21 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater22 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater23 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater24 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater25 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater26 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater27 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater28 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater29 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater30 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater31 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater32 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- COLOR ------

python vprocessor.py --run ../data/skater.avi --out exp/skater33 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater34 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater35 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater36 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater37 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater38 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater39 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater40 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater41 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater42 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater43 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater44 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater45 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater46 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater47 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater48 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- 5 x 5 ------

python vprocessor.py --run ../data/skater.avi --out exp/skater49 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater50 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater51 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater52 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater53 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater54 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater55 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater56 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater57 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater58 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater59 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater60 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/skater.avi --out exp/skater61 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater62 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater63 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/skater.avi --out exp/skater64 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 100 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------
