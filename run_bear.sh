python vprocessor.py --run ../data/bear.mp4 --out exp/bear1 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear2 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear3 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear4 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear5 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear6 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear7 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear8 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear9 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear10 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear11 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear12 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear13 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear14 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear15 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear16 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- 5 x 5 ------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear17 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear18 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear19 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear20 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear21 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear22 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear23 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear24 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear25 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear26 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear27 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear28 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear29 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear30 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear31 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear32 --gray 1 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- COLOR ------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear33 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear34 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear35 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear36 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear37 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear38 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear39 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear40 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear41 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear42 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear43 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear44 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear45 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear46 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear47 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear48 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 3 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

# ---- 5 x 5 ------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear49 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear50 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear51 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear52 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 5.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear53 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear54 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear55 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear56 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 1.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear57 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear58 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear59 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear60 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 10.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------

python vprocessor.py --run ../data/bear.mp4 --out exp/bear61 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 20.0 --lambda0 20.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear62 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 10.0 --lambda0 10.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear63 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 1.0 --lambda0 1.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

python vprocessor.py --run ../data/bear.mp4 --out exp/bear64 --gray 0 --save_scores_only 1 --res 240x180 --day_only 1 --rho 1.0 --check_params 0 --rep 10 --theta 0.0 --beta 1.0 --gamma 1.0 --alpha 1.0 --eta 1.0 --eps1 10000 --eps2 10000 --eps3 10000 --all_black 0 --grad 1 --m 10 --f 5 --init_q 0.5 --k 0.001 --lambda1 100.0 --lambda0 100.0 --lambdaM 0.0 --lambdaE 1.0 --lambdaC 100.0 --step_size 0.000001 --step_adapt 1 --probA 0.9 --probB 1.1

# ----------